﻿module App {
    "use strict";
    export class AppBuilder {


        app: ng.IModule;

        constructor(name: string) {
            this.app = angular.module(name, [
                // Angular modules 
                "ngAnimate",
                "ngRoute",
                "ngSanitize",
                "ngResource"
            ]);
        }

        public start() {
            $(document).ready(() => {
                console.log("booting " + this.app.name);
                angular.bootstrap(document, [this.app.name]);
            });
        }
    }
}